const amplifyconfig = ''' {
    "UserAgent": "aws-amplify-cli/2.0",
    "Version": "1.0",
    "auth": {
        "plugins": {
            "awsCognitoAuthPlugin": {
                "UserAgent": "aws-amplify-cli/0.1.0",
                "Version": "0.1.0",
                "IdentityManager": {
                    "Default": {}
                },
                "CredentialsProvider": {
                    "CognitoIdentity": {
                        "Default": {
                            "PoolId": "eu-central-1:a69b2fcb-6350-4c7c-8677-3a2284421765",
                            "Region": "eu-central-1"
                        }
                    }
                },
                "CognitoUserPool": {
                    "Default": {
                        "PoolId": "eu-central-1_QNsJyBrWj",
                        "AppClientId": "66rcle210jsee3anvn0tcesbau",
                        "AppClientSecret": "f7bmed313spcnfg4jeck2a8cqiueoi14g53mcs1ot7mfovbf1bq",
                        "Region": "eu-central-1"
                    }
                },
                "Auth": {
                    "Default": {
                        "authenticationFlowType": "USER_SRP_AUTH"
                    }
                },
                "PinpointAnalytics": {
                    "Default": {
                        "AppId": "a81d284df65c456bac419d0ce97debbd",
                        "Region": "eu-central-1"
                    }
                },
                "PinpointTargeting": {
                    "Default": {
                        "Region": "eu-central-1"
                    }
                }
            }
        }
    },
    "analytics": {
        "plugins": {
            "awsPinpointAnalyticsPlugin": {
                "pinpointAnalytics": {
                    "appId": "a81d284df65c456bac419d0ce97debbd",
                    "region": "eu-central-1"
                },
                "pinpointTargeting": {
                    "region": "eu-central-1"
                }
            }
        }
    }
}''';