import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gin_rating_app/auth_service.dart';

class VerificationController extends GetxController {

  AuthService _authService = Get.find();

  var verificationCodeController = TextEditingController();

  void verify(String userName) async {
    _authService.verifyCode(userName, verificationCodeController.text);
  }

}

class VerificationPage extends StatelessWidget {

  String _userName;

  @override
  Widget build(BuildContext context) {

    _userName = Get.parameters['userName'];
    return Scaffold(
      body: SafeArea(
        minimum: EdgeInsets.symmetric(horizontal: 40),
        child: _verificationForm(context),
      ),
    );
  }

  Widget _verificationForm(BuildContext context) {


    return GetBuilder<VerificationController>(
        init: VerificationController(),
        builder: (controller) => Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        TextField(
          controller: controller.verificationCodeController,
          decoration: InputDecoration(
              icon: Icon(Icons.confirmation_number),
              labelText: 'Verification code'),
        ),
        FlatButton(
            onPressed: () => controller.verify(_userName),
            child: Text('Verify'),
            color: Theme.of(context).accentColor)
      ],
    ));
  }

}
