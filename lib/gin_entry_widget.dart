

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:gin_rating_app/analytics_service.dart';

import 'analytics_events.dart';
import 'app_state.dart';

class GinEntryController extends GetxController {

  var entryName = TextEditingController();
  var distillerName = TextEditingController();
  double rating = 3;

  GinEntryController(GinEntry entry) {
    entryName.value = TextEditingValue(
      text: entry.name != null ? entry.name : '',
      selection: TextSelection.fromPosition(
        TextPosition(offset: entry.name != null ? entry.name.length : 0),
      ),
    );
    distillerName.value = TextEditingValue(
      text: entry.distillery != null ? entry.distillery : '',
      selection: TextSelection.fromPosition(
        TextPosition(offset: entry.distillery != null ? entry.distillery.length : 0),
      ),
    );
    rating = entry.rating != null ? entry.rating : 0;
  }


  reset() {
    entryName.clear();
    distillerName.clear();
    rating = 0;
  }
  updateRating(double rating) => this.rating = rating;

  GinEntry saveEntry(File picture) {
    AnalyticsService.log(RatedGinEvent(rating: rating));
    return GinEntry(name: entryName.text.trim(), distillery: distillerName.text.trim(), rating: this.rating, picture: picture);
  }


}

class GinEntryWidget extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    var index = int.parse(Get.parameters['index']);
    AppStateController galleryController = Get.find();
    var entry = galleryController.gallery.elementAt(index);
    return
      Scaffold(
        appBar: AppBar(
        title: Text('Gin Rating App'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: GestureDetector(
                child: Icon(Icons.arrow_back),
                onTap: () => Get.toNamed('/gallery')),
          )],
        ),
    body: GetBuilder<GinEntryController>(
        init: GinEntryController(entry),
        builder: (controller) =>
        Container(
          padding: const EdgeInsets.all(32),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
                Expanded(child: Image.file(entry.picture)),
                Expanded(
                  child: TextField(
                    controller: controller.entryName,
                    decoration: InputDecoration(labelText: 'Gin name'),
                  ),
                ),
                Expanded(
                  child: TextField(
                    controller: controller.distillerName,
                    decoration: InputDecoration(labelText: 'Distillery'),
                  ),
                ),
                Expanded(
                  child: RatingBar.builder(
                    initialRating: entry.rating != null ? entry.rating : 0,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 11,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) =>
                        Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                    onRatingUpdate: (rating) {
                      controller.updateRating(rating);
                    },
                  ),
                ),
                Row(
                  children:
                    [
                      Expanded(child: IconButton(
                      icon: Icon(Icons.save),
                      onPressed: () =>
                          galleryController.updateGinEntry(
                            index,
                            controller.saveEntry(entry.picture)))),
                      Expanded(
                        child: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () =>
                                Get.toNamed('/gallery'),

                      ))])
              ],
            )
          ),
        ));
  }
}

class GinEntry {

  String name;
  String distillery;
  double rating;
  File picture;

  GinEntry({this.name, this.distillery, this.rating, this.picture});
}