import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gin_rating_app/auth_service.dart';

import 'auth_credentials.dart';

class SignUpController extends GetxController {

  AuthService _authService = Get.find();

  String userName;
  String email;
  String password;

  setUserName(String value) => userName = value;
  setEmail(String value) => email = value;
  setPassword(String value) => password = value;

  void signUp() {
    final credentials = SignUpCredentials(username: userName, email: email, password: password);
    _authService.signUpWithCredentials(credentials);
  }

}

class SignUpPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          minimum: EdgeInsets.symmetric(horizontal: 40),
          child: Stack(children: [
            // Sign Up Form
            _signUpForm(context),

            // Login Button
            Container(
              alignment: Alignment.bottomCenter,
              child: FlatButton(
                  onPressed: () => Get.toNamed("/login"),
                  child: Text('Already have an account? Login.')),
            )
          ])),
    );
  }

  Widget _signUpForm(BuildContext context) {
    return GetBuilder<SignUpController>(
        init: SignUpController(),
        builder: (controller ) => Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Username TextField
        TextField(
          onChanged: (value) => controller.setUserName(value),
          decoration:
              InputDecoration(icon: Icon(Icons.person), labelText: 'Username'),
        ),

        // Email TextField
        TextField(
          onChanged: (value) => controller.setEmail(value),
          decoration:
              InputDecoration(icon: Icon(Icons.mail), labelText: 'Email'),
        ),

        // Password TextField
        TextField(
          onChanged: (value) => controller.setPassword(value),
          decoration: InputDecoration(
              icon: Icon(Icons.lock_open), labelText: 'Password'),
          obscureText: true,
          keyboardType: TextInputType.visiblePassword,
        ),

        // Sign Up Button
        FlatButton(
            onPressed: () => controller.signUp(),
            child: Text('Sign Up'),
            color: Theme.of(context).accentColor)
      ],
    ));
  }


}
