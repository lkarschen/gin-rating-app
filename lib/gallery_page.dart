import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gin_rating_app/app_state.dart';
import 'package:gin_rating_app/gin_entry_widget.dart';
import 'package:image_picker/image_picker.dart';

class GalleryController extends GetxController {

  AppStateController _appStateController;

  final List<GinEntry> gallery = List<GinEntry>().obs;

  AppStateController getAppState() {return _appStateController;}

  @override
  void refresh() {
    gallery.clear();
    gallery.addAll(_appStateController.gallery);
    super.refresh();
  }

  @override
  void onInit() {
    _appStateController = Get.find();
    gallery.addAll(_appStateController.gallery);
    super.onInit();
  }
}

class GalleryPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => GalleryPageState();

}

class GalleryPageState extends State<GalleryPage> {

  final _picker = ImagePicker();

  List<GinEntry> cardsList = [];

  GalleryController controller = Get.find();

  @override
  void initState() {
    cardsList = controller.gallery;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gin Rating App'),
        actions: [
          // 4
          // Log Out Button
          Padding(
            padding: const EdgeInsets.all(8),
            child:
                GestureDetector(child: Icon(Icons.logout), onTap: () => Get.toNamed('/login')),
          )
        ],
      ),
      body: GridView.builder(
          gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemCount: cardsList.length,
          itemBuilder: (context, idx) {
            return
              GestureDetector(
                  onTap: () => Get.toNamed("/ginEntry/$idx"),
                  child: Image.file(cardsList.elementAt(idx).picture));
          }),
      bottomNavigationBar: BottomAppBar(
          child: Row(
            children: [
              IconButton(icon: Icon(Icons.photo_camera), onPressed: () => _pickImage(ImageSource.camera)),
              IconButton(icon: Icon(Icons.photo_library), onPressed: () => _pickImage(ImageSource.gallery)),
              IconButton(icon: Icon(Icons.refresh), onPressed: () => _clearGallery())
            ],
          ))
    );
  }

  Future<void> _pickImage(ImageSource source) async {
    PickedFile selected = await _picker.getImage(source: source);
    if (selected != null) {
      controller.getAppState().addGinEntry(GinEntry(picture: File(selected.path)));
      controller.refresh();
      setState(() {
        cardsList = controller.gallery;
      });
    } else {
      print('no image selected');
    }
  }

  void _clearGallery() {
    controller.getAppState().clearGallery();
    controller.refresh();
    setState(() {
      cardsList = controller.gallery;
    });
  }

}
