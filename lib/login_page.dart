
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gin_rating_app/auth_service.dart';

import 'auth_credentials.dart';


class LoginController extends GetxController {

  LoginCredentials credentials;

  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  AuthService _authService = Get.find();

  setUserName(String value) => credentials.username;
  setPassword(String password) => credentials.password;

  void login() {
    credentials = LoginCredentials(
        username: userNameController.text,
        password: passwordController.text);
    _authService.loginWithCredentials(credentials);
  }
}

class LoginPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return GetBuilder<LoginController>(
        init: LoginController(),
        builder: (controller) => Scaffold(

      body: SafeArea(
          minimum: EdgeInsets.symmetric(horizontal: 40),

          child: Stack(children: [

             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                TextField(
                  controller: controller.userNameController,
                  decoration:
                  InputDecoration(icon: Icon(Icons.mail), labelText: 'Username'),
                ),
                TextField(
                  controller: controller.passwordController,
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock_open), labelText: 'Password'),
                  obscureText: true,
                  keyboardType: TextInputType.visiblePassword,
                ),

                // Login Button
                FlatButton(
                    onPressed: () => controller.login(),
                    child: Text('Login'),
                    color: Theme.of(context).accentColor)
              ],
            ),

            // 6
            // Sign Up Button
            Container(
              alignment: Alignment.bottomCenter,
              child: FlatButton(
                  onPressed: () => Get.toNamed('/signUp'),
                  child: Text('Don\'t have an account? Sign up.')),
            )
          ])),
    ));
  }

}
