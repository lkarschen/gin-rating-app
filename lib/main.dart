import 'package:amplify_analytics_pinpoint/amplify_analytics_pinpoint.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:gin_rating_app/auth_service.dart';
import 'package:gin_rating_app/gallery_page.dart';
import 'package:gin_rating_app/gin_entry_widget.dart';
import 'package:gin_rating_app/signup.dart';
import 'package:gin_rating_app/verification_page.dart';

import 'amplifyconfiguration.dart';
import 'app_state.dart';
import 'login_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {

  final _amplify = Amplify();
  AuthService _authService;

  @override
  void initState() {
    _configureAmplify();

    _amplify.addPlugin(
        authPlugins: [AmplifyAuthCognito()],
        analyticsPlugins: [AmplifyAnalyticsPinpoint()]);
    Get.put(AppStateController());
    Get.put(GalleryController());
    _authService = Get.put(AuthService());
    _authService.checkAuthStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Gin Rating App',
      theme: ThemeData(visualDensity: VisualDensity.adaptivePlatformDensity),
      initialRoute: "/login",
      getPages: [
        GetPage(name: '/login', page: () => LoginPage()),
        GetPage(name: '/signUp', page: () => SignUpPage()),
        GetPage(name: '/verification/:userName', page: () => VerificationPage()),
        GetPage(name: '/gallery', page: () => GalleryPage()),
        GetPage(name: '/ginEntry/:index', page: () => GinEntryWidget())
      ],
      home:  Container(
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              ));
  }

  void _configureAmplify() async {
    try {
      await _amplify.configure(amplifyconfig);
      print('Successfully configured Amplify 🎉');
    } catch (e) {
      print('Could not configure Amplify ☠️');
    }
  }
}
