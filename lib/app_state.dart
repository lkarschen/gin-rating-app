
import 'package:get/get.dart';
import 'package:gin_rating_app/analytics_service.dart';
import 'package:gin_rating_app/gin_entry_widget.dart';

import 'analytics_events.dart';

class AppStateController extends GetxController {

  var gallery = List<GinEntry>();

  updateGinEntry(int index, GinEntry entry) {
    gallery[index] = entry;
    update();
  }
  addGinEntry(GinEntry entry) {
    AnalyticsService.log(AddedGinEvent());
    gallery.add(entry);
    update();
  }
  removeGinEntry(GinEntry entry) {
    gallery.remove(entry);
    update();
  }
  clearGallery() {
    gallery.clear();
  }


}