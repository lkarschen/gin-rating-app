import 'dart:async';

import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:get/get.dart';

import 'analytics_events.dart';
import 'analytics_service.dart';
import 'analytics_service.dart';
import 'auth_credentials.dart';

enum AuthFlowStatus { login, signUp, verification, session }

class AuthState {
  final AuthFlowStatus authFlowStatus;

  AuthState({this.authFlowStatus});
}

class AuthService extends GetxController {

  final authStateController = StreamController<AuthState>();

  AuthCredentials _credentials;

  void showSignUp() {
    final state = AuthState(authFlowStatus: AuthFlowStatus.signUp);
    authStateController.add(state);
  }

  void showLogin() {
    final state = AuthState(authFlowStatus: AuthFlowStatus.login);
    authStateController.add(state);
  }

  void loginWithCredentials(AuthCredentials credentials) async {
    try {
      final result = await Amplify.Auth.signIn(
          username: credentials.username, password: credentials.password);

      if (result.isSignedIn) {
        final state = AuthState(authFlowStatus: AuthFlowStatus.session);
        authStateController.add(state);
        AnalyticsService.log(LoginEvent());
        Get.offNamed('/gallery');
      } else {
        print('User could not be signed in');
      }
    } on AuthError catch (authError) {
      print('Could not login - ${authError.cause}');
      var userName = credentials.username;
      if (authError.exceptionList.any((exception) => exception.exception == 'USER_NOT_CONFIRMED')) {
        Get.toNamed('/verification/$userName');
      }
    }
  }

  void signUpWithCredentials(SignUpCredentials credentials) async {
    try {
      final userAttributes = {'email': credentials.email};

      final result = await Amplify.Auth.signUp(
          username: credentials.username,
          password: credentials.password,
          options: CognitoSignUpOptions(userAttributes: userAttributes));

      if (result.isSignUpComplete) {
        var userName = credentials.username;
        AnalyticsService.log(SignUpEvent());
        Get.toNamed('/verification/$userName');
      } else {
        this._credentials = credentials;
        print('Signup failed due to ${result.toString()}');
        final state = AuthState(authFlowStatus: AuthFlowStatus.signUp);
        authStateController.add(state);
        Get.toNamed('/signUp');
      }

    } on AuthError catch (authError) {
      print('Failed to sign up - ${authError.cause}');
    }
  }

  void verifyCode(String userName, String verificationCode) async {
    try {
      final result = await Amplify.Auth.confirmSignUp(
          username: userName, confirmationCode: verificationCode);

      if (result.isSignUpComplete) {
        AnalyticsService.log(VerificationEvent());
        Get.toNamed('/login');
      } else {
        Get.toNamed('/login');
      }
    } on AuthError catch (authError) {
      print('Could not verify code - ${authError.cause}');
    }
  }

  void logOut() async {
    try {
      await Amplify.Auth.signOut();

      Get.toNamed('/login');
    } on AuthError catch (authError) {
      print('Could not log out - ${authError.cause}');
    }
  }

  void checkAuthStatus() async {
    try {
      await Amplify.Auth.fetchAuthSession();

      final state = AuthState(authFlowStatus: AuthFlowStatus.session);
      authStateController.add(state);
    } catch (_) {
      final state = AuthState(authFlowStatus: AuthFlowStatus.login);
      authStateController.add(state);
    }
  }
}
